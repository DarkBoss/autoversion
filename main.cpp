#include "application.h"
#include <iostream>

int main(int argc, char *argv[])
{
    try
    {
        Application a(argc, argv);
        return a.exec();
    }
    catch (std::exception &e)
    {
        std::cerr << "[error] " << e.what() << std::endl;
    }
    catch (...)
    {
        std::cerr << "[error] unknown error happened !" << std::endl;
    }
}

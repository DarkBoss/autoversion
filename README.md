# README #

This tool needs the qt5 suite ATM. (Will get rid of it in future releases).


### Sample Usage ###


```
#!shell

autoversion update -o <c_header_file> -a MyApp -c Blueberry
```



Will output (format is major.minor.build.rev):

* The first time
```
#!shell

0.1.1."1226" alpha (Blueberry)
```

* The second time
```
#!shell

0.1.2."1227" alpha (Blueberry)
```


### Manual ###

* Usage: autoversion [mode] [options]
```
#!shell
This software generates build version information,
and outputs it as a c/c++ header.
```
# Options: #

```
#!shell


  --Vv, --verbose-level <0|1|2>        Shows additionnal infos.
  -V, --verbose                        Shows additionnal infos.
  -o, --output <file>                  Sets output filename.
  -a, --appname <name>                 Sets the project name.
  -s, --stage <alpha|beta|rtm|dev>     Sets the project' stage.
  -c, --codename <MyApp,WhatUWant...>  Sets the project codename (version id).
  -g, --guard <NAME_HPP>               Sets the name of the generated header
                                       guards.
  --major <number>                     Sets the major version.
  --minor <number>                     Sets the minor version.
  --build <number>                     Sets the build number.
  --rev <number>                       Sets the revision number.
  --min-major <number>                 Sets the minimum major version.
  --min-minor <number>                 Sets the minimum minor version.
  --min-build <number>                 Sets the minimum build number.
  --min-rev <number>                   Sets the minimum revision number.
  --max-major <number>                 Sets the maximum major version.
  --max-minor <number>                 Sets the maximum minor version.
  --max-build <number>                 Sets the maximum build number.
  --rev-fmt <format>                   Sets formatting of the rev number.
  -v, --version                        Displays version information.
  -h, --help                           Displays this help.

```
# Available modes: #
```
#!shell

* get	-- returns the current software version.
* set	-- sets the current software version.
* update	-- sets the current software version.
* help	-- displays this help screen.

```
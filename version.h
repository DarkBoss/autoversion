#ifndef VERSION_H
# define VERSION_H

# include <QString>

/*
** This app version
*/
# define APP_NAME        "AutoVersion"
# define VERSION_MAJOR   0
# define VERSION_MINOR   0
# define VERSION_REV     1
# define VERSION_STATUS  "alpha"


/*
** Default values
*/
# define DEFAULT_VERSION_FILE           "version.h"
# define DEFAULT_VERSION_STAGE          "alpha"
# define DEFAULT_VERSION_MAJOR          0
# define DEFAULT_VERSION_MINOR          0
# define DEFAULT_VERSION_REV            0
# define DEFAULT_VERSION_BUILD          0
# define DEFAULT_VERSION_PREFIX         "VERSION"
# define DEFAULT_VERSION_APP_NAME       "MyVersionnedApp"
# define DEFAULT_HEADER_GUARD           DEFAULT_VERSION_PREFIX "_HPP"
# define DEFAULT_VERSION_CODENAME       "LilDragon"

class Version
{
public:

    enum            Infos
    {
        MAJOR,
        MINOR,
        REV,
        BUILD,
        MIN_MAJOR,
        MIN_MINOR,
        MIN_REV,
        MIN_BUILD,
        MAX_MAJOR,
        MAX_MINOR,
        MAX_BUILD,
        FMT_REV,
        INFOS_COUNT
    };
    enum            Format
    {
        FormatShort,
        FormatLong
    };

    Version(const QString &filename_ = DEFAULT_VERSION_FILE, const QString &app_name = DEFAULT_VERSION_APP_NAME, const QString &stage_ = DEFAULT_VERSION_STAGE, const QString &code_name = DEFAULT_VERSION_CODENAME,
            unsigned short maj = DEFAULT_VERSION_MAJOR, unsigned short min = DEFAULT_VERSION_MINOR, unsigned short rev = DEFAULT_VERSION_REV, unsigned short build_ = DEFAULT_VERSION_BUILD,
            const QString &prefix = DEFAULT_VERSION_PREFIX, const QString &guard_name = DEFAULT_HEADER_GUARD);

    QString                 generateDefine(const QString &name, const QString &value) const;
    QString                 generate() const;

    const QString           &getFilename() const;
    const QString           &getAppName() const;
    const QString           &getCodeName() const;
    const QString           &getStage() const;
    const QString           &getPrefix() const;
    const QString           &getGuard() const;

    void                    setFilename(const QString &fname);
    void                    setAppName(const QString &fname);
    void                    setCodeName(const QString &fname);
    void                    setStage(const QString &fname);
    void                    setPrefix(const QString &fname);
    void                    setGuard(const QString &guard);
    const QString           &getNumber(Infos part) const;
    void                    setNumber(Infos part, QString val);
    QString                 getVersionString(Version::Format format = Version::FormatLong) const;

    QString                 parseQuotes(QString str) const;
protected:
    QString         mFilename;
    QString         mAppName;
    QString         mCodeName;
    QString         mStage;
    QString         mPrefix;
    QString         mGuard;

    QString         mInfos[INFOS_COUNT];
};

#endif // VERSION_H

#ifndef APPLICATION_H
#define APPLICATION_H

#include <QCoreApplication>
#include <QCommandLineParser>
#include <stdexcept>
#include "version.h"

extern void    _EXCEPT(const QString &msg, const QString &file, size_t line, const QString &func) throw(std::runtime_error);

#define EXCEPT(msg) _EXCEPT(msg, __FILE__, __LINE__, __FUNCTION__)

enum CommandLineParseResult
{
    CommandLineOk,
    CommandLineError,
    CommandLineVersionRequested,
    CommandLineHelpRequested
};

enum Options
{
    OptionVerboseLevel,
    OptionVerbose,
    OptionOutput,
    OptionMajor,
    OptionMinor,
    OptionBuild,
    OptionRev,
    OptionFmt,
    OptionMinMajor,
    OptionMinMinor,
    OptionMinRev,
    OptionMinBuild,
    OptionMaxMajor,
    OptionMaxMinor,
    OptionMaxBuild,
    OptionAppName,
    OptionCodeName,
    OptionStage,
    OptionGuard,
    OptionHelp,
    OptionVersion,
    OptionCount
};

enum Mode
{
    ModeGet, /* Returns the current build infos */
    ModeSet, /* Sets the build infos from given parameters */
    ModeUpdate, /* Updates the version informations per build */
    ModeCount
};
enum Verbose
{
    VerboseNone,
    VerboseLow, /* only important & errors */
    VerboseNormal, /* Normal verbose messages & errors*/
    VerboseHigh, /* All */
    VerboseCount, /* Number of verbose modes */

};

//extern QCommandLineOption   g_options[OptionCount];

class   Application : public QCoreApplication
{
public:
    Application(int &argc, char *argv[]);

    CommandLineParseResult  parseCommandLine(QCommandLineParser &parser, Version *query, QString *errorMessage);
    int                     modes();
    int                     exec();
    bool                    isVerbose() const;

    void                    saveVersion(Version *vers);
    void                    loadVersion(Version *vers);
protected:
    void    handler_get(Version *vers);
    void    handler_set(Version *vers);
    void    handler_update(Version *vers);

protected:
    Verbose    mVerbose;
};

#endif // APPLICATION_H

#include "version.h"
#include <QTime>

Version::Version(const QString &filename_, const QString &app_name, const QString &stage_, const QString &code_name,
        unsigned short maj, unsigned short min, unsigned short rev, unsigned short build_,
        const QString &prefix, const QString &guard_name)
    : mFilename(filename_)
    , mAppName(app_name)
    , mCodeName(code_name)
    , mStage(stage_)
    , mPrefix(prefix)
    , mGuard(guard_name)
{
    mInfos[MAJOR] = QString::number(maj);
    mInfos[MINOR] = QString::number(min);
    mInfos[REV] = QString::number(rev);
    mInfos[BUILD] = QString::number(build_);
    mInfos[MIN_MAJOR] = "0";
    mInfos[MIN_MINOR] = "0";
    mInfos[MIN_REV] = "0";
    mInfos[MIN_BUILD] = QString::number(build_);
    mInfos[MAX_MAJOR] = "100";
    mInfos[MAX_MINOR] = "10";
    mInfos[MAX_BUILD] = "250";
    mInfos[FMT_REV] = "HHmm";
}

QString         Version::getVersionString(Version::Format format) const
{
    QString fmt;
    switch(format)
    {
    case FormatLong:
        fmt = QString("%1.%2.%3.%4 %5 (%6)")
                .arg(mInfos[MAJOR])
                .arg(mInfos[MINOR])
                .arg(mInfos[BUILD])
                .arg(mInfos[REV])
                .arg(mStage)
                .arg(mCodeName);
        break;
    default:
        fmt = QString("%1.%2.%4%5")
                .arg(mInfos[MAJOR])
                .arg(mInfos[MINOR])
                .arg(mInfos[BUILD])
                .arg(mInfos[REV])
                .arg(mStage.at(0));
    };
    return (fmt);
}

QString         Version::generateDefine(const QString &name, const QString &value) const {
    return QString("# define %1_%2\t%3")
            .arg(mPrefix).arg(name).arg(value);
}
QString         Version::generate() const {
    QString rev(QTime::currentTime().toString(mInfos[Version::FMT_REV]));
    QString core(
            generateDefine("MAJOR", mInfos[MAJOR]) + "\n" +
            generateDefine("MINOR", mInfos[MINOR]) + "\n" +
            generateDefine("BUILD", mInfos[BUILD]) + "\n" +
            generateDefine("REV", "\"" + rev + "\"") + "\n" +
            generateDefine("REV_FMT", mInfos[FMT_REV]) + "\n" +
            generateDefine("MAJOR_MAX", mInfos[MAX_MAJOR]) + "\n" +
            generateDefine("MINOR_MAX", mInfos[MAX_MINOR]) + "\n" +
            generateDefine("BUILD_MAX", mInfos[MAX_BUILD]) + "\n" +
            generateDefine("STRING", "\"" + QString("%1.%2.%3.%4").arg(mInfos[MAJOR]).arg(mInfos[MINOR]).arg(mInfos[BUILD]).arg(rev) + "\"") + "\n" +
            generateDefine("STAGE", "\"" + mStage + "\"") + "\n" +
            generateDefine("STAGE_SHORT", "\"" + QString(mStage.at(0)) + "\"") + "\n" +
            generateDefine("CODE_NAME", "\"" + mCodeName + "\"") + "\n" +
            generateDefine("DATE", "\"" + QDateTime::currentDateTime().toString() + "\"") + "\n" +
            generateDefine("APP_NAME", "\"" + mAppName + "\"") + "\n");
    return QString( "#ifndef %1\n"
                    "# define %1\n\n"
                    "%2\n\n"
                    "#endif /* !%1 */\n")
            .arg(mGuard).arg(core);
}

QString         Version::parseQuotes(QString str) const{
    while (!str.isEmpty() && *str.begin() == '"')
    {
       str.remove(0, 1);
       str = str.trimmed();
    }
    while (!str.isEmpty() && str.at(str.size() - 1) == '"')
    {
       str.remove(str.size() - 1, 1);
       str = str.trimmed();
    }
    return (str);
}

void            Version::setNumber(Infos part, QString val){ mInfos[part] = val; }
void            Version::setFilename(const QString &fname) { mFilename = parseQuotes(fname); }
void            Version::setAppName(const QString &name){ mAppName = parseQuotes(name); }
void            Version::setCodeName(const QString &name){ mCodeName = parseQuotes(name); }
void            Version::setStage(const QString &stage) { mStage = parseQuotes(stage); }
void            Version::setPrefix(const QString &prefix){ mPrefix = parseQuotes(prefix); }
void            Version::setGuard(const QString &guard){ mGuard = parseQuotes(guard); }

const QString &Version::getFilename() const { return (mFilename); }
const QString &Version::getGuard() const { return (mGuard); }
const QString &Version::getAppName() const { return (mAppName); }
const QString &Version::getCodeName() const { return (mCodeName); }
const QString &Version::getStage() const { return (mStage); }
const QString &Version::getPrefix() const { return (mPrefix); }
const QString &Version::getNumber(Infos part) const { return (mInfos[part]); }

#include "application.h"
#include <iostream>

using std::cout;
using std::cerr;
using std::endl;


CommandLineParseResult Application::parseCommandLine(QCommandLineParser &parser, Version *vers, QString *errorMessage)
{
    parser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsLongOptions);

    QCommandLineOption vl( QStringList() << "Vv" << "verbose-level", QCoreApplication::tr("Shows additionnal infos.", "Application"), QString("0|1|2") );
    parser.addOption(vl); QCommandLineOption v( QStringList() << "V" << "verbose", QCoreApplication::tr("Shows additionnal infos.", "Application"), QString() );
    parser.addOption(v); QCommandLineOption o( QStringList() << "o" << "output", QCoreApplication::tr("Sets output filename.", "Application"), QString("file") );
    parser.addOption(o); QCommandLineOption a( QStringList() << "a" << "appname", QCoreApplication::tr("Sets the project name.", "Application"), QString("name") );
    parser.addOption(a); QCommandLineOption s( QStringList() << "s" << "stage", QCoreApplication::tr("Sets the project' stage.", "Application"), QString("alpha|beta|rtm|dev") );
    parser.addOption(s); QCommandLineOption c( QStringList() << "c" << "codename", QCoreApplication::tr("Sets the project codename (version id).", "Application"), QString("MyApp,WhatUWant...") );
    parser.addOption(c); QCommandLineOption g( QStringList() << "g" << "guard", QCoreApplication::tr("Sets the name of the generated header guards.", "Application"), QString("NAME_HPP") );
    parser.addOption(g); QCommandLineOption maj( QStringList() << "major", QCoreApplication::tr("Sets the major version.", "Application"), QString("number") );
    parser.addOption(maj); QCommandLineOption min( QStringList() << "minor", QCoreApplication::tr("Sets the minor version.", "Application"), QString("number") );
    parser.addOption(min); QCommandLineOption b( QStringList() << "build", QCoreApplication::tr("Sets the build number.", "Application"), QString("number") );
    parser.addOption(b); QCommandLineOption r( QStringList() << "rev", QCoreApplication::tr("Sets the revision number.", "Application"), QString("number") );
    parser.addOption(r); QCommandLineOption mmaj( QStringList() << "min-major", QCoreApplication::tr("Sets the minimum major version.", "Application"), QString("number") );
    parser.addOption(mmaj); QCommandLineOption mmin( QStringList() << "min-minor", QCoreApplication::tr("Sets the minimum minor version.", "Application"), QString("number") );
    parser.addOption(mmin); QCommandLineOption mb( QStringList() << "min-build", QCoreApplication::tr("Sets the minimum build number.", "Application"), QString("number") );
    parser.addOption(mb); QCommandLineOption mr( QStringList() << "min-rev", QCoreApplication::tr("Sets the minimum revision number.", "Application"), QString("number") );
    parser.addOption(mr); QCommandLineOption Mmaj( QStringList() << "max-major", QCoreApplication::tr("Sets the maximum major version.", "Application"), QString("number") );
    parser.addOption(Mmaj); QCommandLineOption Mmin( QStringList() << "max-minor", QCoreApplication::tr("Sets the maximum minor version.", "Application"), QString("number") );
    parser.addOption(Mmin); QCommandLineOption Mb( QStringList() << "max-build", QCoreApplication::tr("Sets the maximum build number.", "Application"), QString("number") );
    parser.addOption(Mb); QCommandLineOption fr( QStringList() << "rev-fmt", QCoreApplication::tr("Sets formatting of the rev number.", "Application"), QString("format") );
    parser.addOption(fr);

    QCommandLineOption versc(parser.addVersionOption());
    QCommandLineOption h(parser.addHelpOption());

    if (!parser.parse(QCoreApplication::arguments())) {
        *errorMessage = parser.errorText();
        return CommandLineError;
    }

    if (parser.isSet(h) || arguments().size() < 2)
        return CommandLineHelpRequested;

    if (parser.isSet(versc))
        return CommandLineVersionRequested;

    if (parser.isSet(v))
        mVerbose = VerboseLow;

    if (parser.isSet(vl))
    {
        int lvl = parser.value(vl).toInt();
        if (lvl < 0) lvl = 0;
        if (lvl >= VerboseCount) lvl = VerboseCount - 1;
        mVerbose = Verbose(lvl);
    }


    if (parser.isSet(o))
        vers->setFilename(parser.value(o));
    try{loadVersion(vers);}
    catch(std::exception &e)
    {cerr << "[error] " << e.what() << endl;}
    catch(...)
    {cerr << "[error] unknown error !" << endl;}
    if (parser.isSet(g))
        vers->setGuard(parser.value(g));

    if (parser.isSet(a))
        vers->setAppName(parser.value(a));

    if (parser.isSet(Mmaj))
        vers->setNumber(Version::MAX_MAJOR, parser.value(Mmaj));
    if (parser.isSet(Mmin))
        vers->setNumber(Version::MAX_MINOR, parser.value(Mmin));
    if (parser.isSet(Mb))
        vers->setNumber(Version::MAX_BUILD, parser.value(Mb));

    if (parser.isSet(mmaj))
        vers->setNumber(Version::MIN_MAJOR, parser.value(mmaj));
    if (parser.isSet(mmin))
        vers->setNumber(Version::MIN_MINOR, parser.value(mmin));
    if (parser.isSet(mb))
        vers->setNumber(Version::MIN_BUILD, parser.value(mb));
    if (parser.isSet(mr))
        vers->setNumber(Version::MIN_REV, parser.value(mr));

    if (parser.isSet(maj))
        vers->setNumber(Version::MAJOR, parser.value(maj));
    if (parser.isSet(min))
        vers->setNumber(Version::MINOR, parser.value(min));
    if (parser.isSet(b))
        vers->setNumber(Version::BUILD, parser.value(b));
    if (parser.isSet(r))
        vers->setNumber(Version::REV, parser.value(r));
    if (parser.isSet(fr))
        vers->setNumber(Version::FMT_REV, parser.value(fr));


    vers->setNumber(Version::MAJOR, std::max(vers->getNumber(Version::MAJOR), vers->getNumber(Version::MIN_MAJOR)));
    vers->setNumber(Version::MINOR, std::max(vers->getNumber(Version::MINOR), vers->getNumber(Version::MIN_MINOR)));
    vers->setNumber(Version::BUILD, std::max(vers->getNumber(Version::BUILD), vers->getNumber(Version::MIN_BUILD)));

    if (parser.isSet(s))
        vers->setStage(parser.value(s));

    if (parser.isSet(c))
        vers->setCodeName(parser.value(c));

    return CommandLineOk;
}

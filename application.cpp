#include "application.h"
#include <iostream>
#include <QStringList>
#include <stdio.h>
using std::cout;
using std::cerr;
using std::endl;

void    _EXCEPT(const QString &msg, const QString &file, size_t line, const QString &func) throw(std::runtime_error)
{
    QString str(QString("%1 from function '%2' (at line %3 in file %4)")
        .arg(msg).arg(func).arg(line)
        .arg(file));
    throw std::runtime_error(str.toStdString());
}

Application::Application(int &argc, char *argv[])
    : QCoreApplication(argc, argv)
    , mVerbose(VerboseNone)
{
    QCoreApplication::setApplicationName(APP_NAME);
    QCoreApplication::setApplicationVersion(QString("%1.%2.%3%4").arg(VERSION_MINOR).arg(VERSION_MAJOR).arg(VERSION_REV).arg(VERSION_STATUS));
}

bool    Application::isVerbose() const {return mVerbose;}

int     Application::exec()
{
    QCommandLineParser parser;
    parser.setApplicationDescription(tr("This software generates build version information,\nand outputs it as a c/c++ header."));
    Version vers;
    QString errorMessage;
    switch (parseCommandLine(parser, &vers, &errorMessage)) {
    case CommandLineOk:
        break;
    case CommandLineError:
        std::cerr << errorMessage.toStdString() << std::endl;
        std::cerr << parser.helpText().toStdString() << std::endl;
        return (EXIT_FAILURE);
    case CommandLineVersionRequested:
        printf("%s %s\n", qPrintable(QCoreApplication::applicationName()),
               qPrintable(QCoreApplication::applicationVersion()));
        return (EXIT_SUCCESS);
    case CommandLineHelpRequested:
        cout << parser.helpText().toStdString() << endl;
        modes();
        return (EXIT_FAILURE);
    }
    if (mVerbose >= VerboseLow)
        cout << "[info] Starting application ..." << endl;
    if (mVerbose >= VerboseHigh)
    {
        if (!vers.getFilename().isEmpty())
            cout << "\t" << "Output file: " << vers.getFilename().toStdString() << endl;
        if (!vers.getGuard().isEmpty())
            cout << "\t" << "Header-guards: " << vers.getGuard().toStdString() << endl;
        if (!vers.getPrefix().isEmpty())
            cout << "\t" << "Prefix (variables): " << vers.getPrefix().toStdString() << endl;
        if (!vers.getCodeName().isEmpty())
            cout << "\t" << "Codename: " << vers.getCodeName().toStdString() << endl;
        if (!vers.getAppName().isEmpty())
            cout << "\t" << "Application name: " << vers.getAppName().toStdString() << endl;
        if (!vers.getStage().isEmpty())
            cout << "\t" << "Stage: " << vers.getStage().toStdString() << endl;
    }
    const QStringList args = parser.positionalArguments();
    if (args.size() > 1)
    {
        cerr << "[error] too much modes given: " << args.join(", ").toStdString() << endl;
        return (EXIT_FAILURE);
    }
    if (args[0] == "get")
        handler_get(&vers);
    else if (args[0] == "set")
        handler_set(&vers);
    else if (args[0] == "update")
        handler_update(&vers);
    else
    {
        cerr << "[error] invalid program mode." << endl << "\tmust be one of (get|set|update)" << endl;
        return (EXIT_FAILURE);
    }
    if (mVerbose >= VerboseLow)
        cout << "[info] Shutting down application ..." << endl;
    return (EXIT_SUCCESS);
}

#include "application.h"
#include <iostream>
#include <QFile>
#include <QTime>
#include <QTime>

using std::cout;
using std::cerr;
using std::endl;

void     Application::saveVersion(Version *vers)
{
    QString fname = vers->getFilename();
    QString gen(
                QString("/*\n") +
                QString("** auto generated file from %1 %2.%3 (%4)\n")
                    .arg(APP_NAME).arg(VERSION_MAJOR).arg(VERSION_MINOR).arg(VERSION_STATUS) +
                QString("** at %1 on %2\n")
                    .arg(QTime::currentTime().toString()).arg(QDate::currentDate().toString()) +
                QString("*/\n") +
                QString("\n") +
                vers->generate());
    if (mVerbose >= VerboseNormal)
    {
        if (fname.isEmpty())
            cout << "[info] output mode is STDOUT" << endl;
        else
            cout << "[info] output mode is file (" << fname.toStdString() << ")" << endl;
    }
    if (fname.isEmpty())
        cout << gen.toStdString() << endl;
    else
    {
        QFile f(fname);
        if (!f.open(QFile::WriteOnly | QFile::Text))
            EXCEPT("cannot open file " + vers->getFilename() + " for writing.");
        f.write(gen.toUtf8());
    }
}

void        Application::loadVersion(Version *vers)
{
    if (vers->getFilename().isEmpty())
        return;
    QFile f(vers->getFilename());
    if (!f.open(QFile::ReadOnly | QFile::Text))
        EXCEPT("cannot open file " + vers->getFilename() + " for reading.");
    QString line;
    unsigned long line_id = 0;
    QStringList words;
    QString word;
    if (mVerbose >= VerboseHigh)
        cout << "[info] current configuration: "<< endl;
    while (!f.atEnd())
    {
        line = f.readLine().trimmed();
        line_id++;
        if (!line.isEmpty() && line[0] == '#')
        {
            line.remove(0, 1);
            line = line.trimmed();
            if (line.left(6) == "define")
            {
                line = line.mid(7).trimmed();
                QRegExp rx("(\\ |\\t)");
                words = line.split(rx);
                if (!words.empty())
                {
                    if (words[0] == QString("%1_%2").arg(vers->getPrefix()).arg("MAJOR"))
                    {
                        if (words.size() >= 2)
                        {
                            if (mVerbose >= VerboseHigh) cout << "\t[info] Version Major: " << words[1].trimmed().toStdString() << endl;
                            vers->setNumber(Version::MAJOR, words[1].trimmed());
                        }
                        else
                            cerr << "[warning] badly formed line(" << std::to_string(line_id) << "): " << line.toStdString() << endl;
                    }
                    else if (words[0] == QString("%1_%2").arg(vers->getPrefix()).arg("MINOR"))
                    {
                        if (words.size() >= 2)
                        {
                            if (mVerbose >= VerboseHigh) cout << "\t[info] Version Minor: " << words[1].trimmed().toStdString() << endl;
                            vers->setNumber(Version::MINOR, words[1].trimmed());
                        }
                        else
                            cerr << "[warning] badly formed line(" << std::to_string(line_id) << "): " << line.toStdString() << endl;
                    }
                    else if (words[0] == QString("%1_%2").arg(vers->getPrefix()).arg("REV"))
                    {
                        if (words.size() >= 2)
                        {
                            if (mVerbose >= VerboseHigh) cout << "\t[info] Version Rev: " << words[1].trimmed().toStdString() << endl;
                            vers->setNumber(Version::REV, words[1].trimmed());
                        }
                        else
                            cerr << "[warning] badly formed line(" << std::to_string(line_id) << "): " << line.toStdString() << endl;
                    }
                    else if (words[0] == QString("%1_%2").arg(vers->getPrefix()).arg("BUILD"))
                    {
                        if (words.size() >= 2)
                        {
                            if (mVerbose >= VerboseHigh) cout << "\t[info] Version Build: " << words[1].trimmed().toStdString() << endl;
                            vers->setNumber(Version::BUILD, words[1].trimmed());
                        }
                        else
                            cerr << "[warning] badly formed line(" << std::to_string(line_id) << "): " << line.toStdString() << endl;
                    }
                    else if (words[0] == QString("%1_%2").arg(vers->getPrefix()).arg("REV_FMT"))
                    {
                        if (words.size() >= 2)
                        {
                            if (mVerbose >= VerboseHigh) cout << "\t[info] Version Rev Format: " << words[1].trimmed().toStdString() << endl;
                            vers->setNumber(Version::FMT_REV, words[1].trimmed());
                        }
                        else
                            cerr << "[warning] badly formed line(" << std::to_string(line_id) << "): " << line.toStdString() << endl;
                    }
                    else if (words[0] == QString("%1_%2").arg(vers->getPrefix()).arg("MAJOR_MAX"))
                    {
                        if (words.size() >= 2)
                        {
                            if (mVerbose >= VerboseHigh) cout << "\t[info] Version Max Major: " << words[1].trimmed().toStdString() << endl;
                            vers->setNumber(Version::MAX_MAJOR, words[1].trimmed());
                        }
                        else
                            cerr << "[warning] badly formed line(" << std::to_string(line_id) << "): " << line.toStdString() << endl;
                    }
                    else if (words[0] == QString("%1_%2").arg(vers->getPrefix()).arg("MINOR_MAX"))
                    {
                        if (words.size() >= 2)
                        {
                            if (mVerbose >= VerboseHigh) cout << "\t[info] Version Max Minor: " << words[1].trimmed().toStdString() << endl;
                            vers->setNumber(Version::MAX_MINOR, words[1].trimmed());
                        }
                        else
                            cerr << "[warning] badly formed line(" << std::to_string(line_id) << "): " << line.toStdString() << endl;
                    }
                    else if (words[0] == QString("%1_%2").arg(vers->getPrefix()).arg("BUILD_MAX"))
                    {
                        if (words.size() >= 2)
                        {
                            if (mVerbose >= VerboseHigh) cout << "\t[info] Version Max Build: " << words[1].trimmed().toStdString() << endl;
                            vers->setNumber(Version::MAX_BUILD, words[1].trimmed());
                        }
                        else
                            cerr << "[warning] badly formed line(" << std::to_string(line_id) << "): " << line.toStdString() << endl;
                    }
                    else if (words[0] == QString("%1_%2").arg(vers->getPrefix()).arg("STAGE"))
                    {
                        if (words.size() >= 2)
                        {
                            if (mVerbose >= VerboseHigh) cout << "\t[info] Version Stage: " << words[1].trimmed().toStdString() << endl;
                            vers->setStage(words[1].trimmed());
                        }
                        else
                            cerr << "[warning] badly formed line(" << std::to_string(line_id) << "): " << line.toStdString() << endl;
                    }
                    else if (words[0] == QString("%1_%2").arg(vers->getPrefix()).arg("STAGE_SHORT"))
                    {
                        if (words.size() >= 2)
                        {
                            if (mVerbose >= VerboseHigh) cout << "\t[info] Version Stage Short: " << words[1].trimmed().toStdString() << endl;
                        }
                        else
                            cerr << "[warning] badly formed line(" << std::to_string(line_id) << "): " << line.toStdString() << endl;
                    }
                    else if (words[0] == QString("%1_%2").arg(vers->getPrefix()).arg("CODE_NAME"))
                    {
                        if (words.size() >= 2)
                        {
                            if (mVerbose >= VerboseHigh) cout << "\t[info] Version Codename: " << words[1].trimmed().toStdString() << endl;
                            vers->setCodeName(words[1].trimmed());
                        }
                        else
                            cerr << "[warning] badly formed line(" << std::to_string(line_id) << "): " << line.toStdString() << endl;
                    }
                    else if (words[0] == QString("%1_%2").arg(vers->getPrefix()).arg("APP_NAME"))
                    {
                        if (words.size() >= 2)
                        {
                            if (mVerbose >= VerboseHigh) cout << "\t[info] Application Name: " << words[1].trimmed().toStdString() << endl;
                            vers->setAppName(words[1].trimmed());
                        }
                        else
                            cerr << "[warning] badly formed line(" << std::to_string(line_id) << "): " << line.toStdString() << endl;
                    }
                }
            }
        }
        line_id++;
    }
}

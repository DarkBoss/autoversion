#include "application.h"
#include <iostream>

using std::cout;
using std::cerr;
using std::endl;

int     Application::modes()
{
    cerr << "\tavailable modes:" << endl;
    cerr << "\t\tget\t-- returns the current software version." << endl;
    cerr << "\t\tset\t-- sets the current software version." << endl;
    cerr << "\t\tupdate\t-- sets the current software version." << endl;
    cerr << "\t\thelp\t-- displays this help screen." << endl;
    return (EXIT_FAILURE);
}

void    Application::handler_get(Version *vers) {
    if (mVerbose >= VerboseLow)
        cout << "[info] getting app version" << endl;
    if (vers->getFilename().isEmpty())
        vers->setFilename(DEFAULT_VERSION_FILE);
    loadVersion(vers);
    cout << vers->getVersionString().toStdString() << endl;
    (void)vers;
}

void    Application::handler_set(Version *vers) {
    if (mVerbose >= VerboseLow)
        cout << "[info] setting app version" << endl;
    saveVersion(vers);
}

void    Application::handler_update(Version *vers) {
    if (mVerbose >= VerboseLow)
        cout << "[info] updating app version:" << endl << "\tfrom " << vers->getVersionString().toStdString() << endl << "\tto ";
    vers->setNumber(Version::BUILD, QString::number(vers->getNumber(Version::BUILD).toUShort() + 1));
    vers->setNumber(Version::MINOR, QString::number(vers->getNumber(Version::BUILD).toUShort() / vers->getNumber(Version::MAX_BUILD).toUShort() + 1));
    vers->setNumber(Version::MAJOR, QString::number(vers->getNumber(Version::MINOR).toUShort() / vers->getNumber(Version::MAX_MINOR).toUShort()));
    if (vers->getNumber(Version::MAJOR).toUShort() > vers->getNumber(Version::MAX_MAJOR).toUShort())
        EXCEPT("maximum major versions reached!");
    cout << vers->getVersionString().toStdString() << endl;
    saveVersion(vers);
}
